///-----------------------------------------------------------------
///   Author : Théo Sabattié                    
///   Date   : 15/01/2020 16:39
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.IsartDigital.Common.ScriptableObjects
{
	[CreateAssetMenu(menuName = "Common/AnimatorParameter")]
	public class AnimatorParameter : ScriptableObject
	{
		private int _id;
		public int ID => _id;

		private void OnEnable()
		{
			_id = Animator.StringToHash(name);
		}

		public static implicit operator int(AnimatorParameter animatorParameter)
		{
			return animatorParameter._id;
		}
	}
}