///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 29/01/2020 16:40
///-----------------------------------------------------------------

using System;
using UnityEngine;
using UnityEngine.Events;

namespace Com.Onigiriz.Common
{
	[Serializable] public class TransformUnityEvent : UnityEvent<Transform> {}
}