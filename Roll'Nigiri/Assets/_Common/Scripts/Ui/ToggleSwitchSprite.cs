///-----------------------------------------------------------------
///   Author : Théo Sabattié                    
///   Date   : 08/10/2019 09:45
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.Onigiriz.Common.UI
{
	[RequireComponent(typeof(Toggle)), ExecuteInEditMode]
	public class ToggleSwitchSprite : MonoBehaviour
	{
		[SerializeField] private Sprite spriteIsOff = null;
		private Sprite spriteIsOn = null;
		private Image image = null;

		private void Start()
		{
			Toggle toggle = GetComponent<Toggle>();
			image = toggle.image;
			spriteIsOn = image.sprite;

			toggle.onValueChanged.AddListener(Toggle_OnValueChanged);
		}

		private void Toggle_OnValueChanged(bool isOn)
		{
			image.sprite = isOn ? spriteIsOn : spriteIsOff;
		}
	}
}