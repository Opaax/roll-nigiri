///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 29/01/2020 16:33
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

namespace Com.Onigiriz.Common
{
	[RequireComponent(typeof(Collider2D))]
	public class Trigger2D : MonoBehaviour
	{
		[SerializeField] private TransformUnityEvent _onEnter = null;
		[SerializeField] private TransformUnityEvent _onExit = null;

		public event UnityAction<Transform> OnEnter
		{
			add { _onEnter.AddListener(value); }
			remove { _onEnter.RemoveListener(value); }
		}

		public event UnityAction<Transform> OnExit
		{
			add { _onExit.AddListener(value); }
			remove { _onExit.RemoveListener(value); }
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			_onEnter.Invoke(collision.transform);
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			_onExit.Invoke(collision.transform);
		}
	}
}