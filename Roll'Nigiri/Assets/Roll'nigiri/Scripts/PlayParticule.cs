///-----------------------------------------------------------------
/// Author : Arthur RAFFAUD
/// Date : 01/02/2020 05:16
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri {
	public class PlayParticule : MonoBehaviour {
        [SerializeField] private ParticleSystem particle;
		private void Start () {
			
		}

        private void StartParticle()
        {
            particle.Play();
        }
		
		private void Update () {
			
		}
	}
}