///-----------------------------------------------------------------
/// Author : Enguerran COBERT
/// Date : 02/02/2020 05:18
///-----------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri {
	public class LevelGenerator : MonoBehaviour {
        [Header("Settings")]
        [SerializeField] private LevelGeneratorSettings settings = null;

		[SerializeField] private List<GameObject> all = new List<GameObject>();

        private void Start()
        {
            //Generate();
        }

        private void Update()
        {
           
        }
        public void Generate()
        {
            //GenerateRice();
            StartCoroutine(GenerateRice2());
            GenerateSushi();
            GenerateTuna();
            GenerateNori();
        }

		public void UnGenerate()
		{
			for (int i = all.Count - 1; i >= 0; i--)
			{
				Destroy(all[i]);
			}
		}

		private void GenerateNori()
        {
            for (int i = 0; i < settings.NbNoriToSpawn; i++)
            {
                GameObject lNori = Instantiate(settings.NoriPrefab);

                Vector3 lPoint = Quaternion.AngleAxis(90f, Vector3.right) * UnityEngine.Random.insideUnitCircle;
                Vector3 lDirection = lPoint - transform.position;
                Vector3 lOffSet = lDirection.normalized * 7f;
                float lDistance = UnityEngine.Random.Range(7f, 15f);

                lNori.transform.position = lOffSet;

				all.Add(lNori);

                lNori.GetComponent<Rigidbody>().AddForce(lDirection * lDistance,ForceMode.Impulse);
                //lTuna.transform.position = lOffSet + lDirection * lDistance;
                //lTuna.transform.position = new Vector3(lTuna.transform.position.x, transform.position.y, lTuna.transform.position.z);
            }
        }

        private void GenerateTuna()
        {
            for (int i = 0; i < settings.NbTunaToSpawn; i++)
            {
                GameObject lTuna = Instantiate(settings.TunaPrefab);

                Vector3 lPoint = Quaternion.AngleAxis(90f, Vector3.right) * UnityEngine.Random.insideUnitCircle;
                Vector3 lDirection = lPoint - transform.position;
                Vector3 lOffSet = lDirection.normalized * 7f;
                float lDistance = UnityEngine.Random.Range(7f, 15f);

                lTuna.transform.position = lOffSet;
				all.Add(lTuna);

				lTuna.GetComponent<Rigidbody>().AddForce(lDirection * lDistance, ForceMode.Impulse);

                //lTuna.transform.position = lOffSet + lDirection * lDistance;
                //lTuna.transform.position = new Vector3(lTuna.transform.position.x, transform.position.y, lTuna.transform.position.z);
            }
        }

        private void GenerateSushi()
        {
            for (int i = 0; i < settings.NbSushiToSpawn; i++)
            {
                GameObject lSuhsi = Instantiate(settings.SushiPrefab);

                Vector3 lPoint = Quaternion.AngleAxis(90f, Vector3.right) * UnityEngine.Random.insideUnitCircle;
                Vector3 lDirection = lPoint - transform.position;
                Vector3 lOffSet = lDirection.normalized * 7f;
                float lDistance = UnityEngine.Random.Range(7f, 15f);

                lSuhsi.transform.position = lOffSet;

                lSuhsi.GetComponent<Rigidbody>().AddForce(lDirection * lDistance, ForceMode.Impulse);
                //lSuhsi.transform.position = lOffSet + lDirection * lDistance;
                //lSuhsi.transform.position = new Vector3(lSuhsi.transform.position.x, transform.position.y, lSuhsi.transform.position.z);
            }
        }

        private void GenerateRice()
        {
            for (int i = 0; i < settings.NbRiceToSpawn; i++)
            {
                GameObject lRice = Instantiate(settings.RicePrefab);

                Vector3 lPoint = Quaternion.AngleAxis(90f, Vector3.right) * UnityEngine.Random.insideUnitCircle;
                Vector3 lDirection = lPoint - transform.position;
                Vector3 lOffSet = lDirection.normalized * 2f;
                float lDistance = UnityEngine.Random.Range(5f, 15f);

                lRice.transform.position = lOffSet;

                lRice.GetComponent<Rigidbody>().AddForce(lDirection * lDistance, ForceMode.Impulse);
				all.Add(lRice);

				//lRice.transform.position = lOffSet + lDirection * lDistance;
			}
		}

        private IEnumerator GenerateRice2()
        {
            for (int i = 0; i < settings.NbRiceToSpawn; i++)
            {
                GameObject lRice = Instantiate(settings.RicePrefab);

                Vector3 lPoint = Quaternion.AngleAxis(90f, Vector3.right) * UnityEngine.Random.insideUnitCircle;
                Vector3 lDirection = lPoint - transform.position;
                Vector3 lOffSet = lDirection.normalized * 2f;
                float lDistance = UnityEngine.Random.Range(5f, 15f);

                lRice.transform.position = lOffSet;

                lRice.GetComponent<Rigidbody>().AddForce(lDirection * lDistance, ForceMode.Impulse);
                all.Add(lRice);

                if(i % 30 == 0)
                    yield return new WaitForSeconds(0.01f);
            }
        }
    }
}