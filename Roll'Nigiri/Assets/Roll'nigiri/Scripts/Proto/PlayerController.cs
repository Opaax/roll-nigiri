///-----------------------------------------------------------------
/// Author : Enguerran COBERT
/// Date : 31/01/2020 23:37
///-----------------------------------------------------------------

using Com.Onigiriz.RollNigiri.PlayerScripts;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri.RollNigiri.Proto {
	public class PlayerController : MonoBehaviour {
        [Header("Specs")]
        [SerializeField] private Rigidbody rb = null;
        [SerializeField,Range(1f,20f)] private float speed = 10f;
        [Space]
        [Header("Settings")]
        [SerializeField] private PlayerControllerSettings settings = null;

        private float startZAcceleromètre = 0;
        private float startXAcceleromètre = 0;

        private bool isRolling = false;

        private Vector3 _direction = Vector3.zero;

		private void Start () {
#if UNITY_ANDROID && !UNITY_EDITOR
            Calib();  
#endif
        }

        private void Update () {
#if UNITY_ANDROID && !UNITY_EDITOR
            _direction = new Vector3(Input.acceleration.x - startXAcceleromètre , 0, -Input.acceleration.z + startZAcceleromètre );
#else
            _direction = new Vector3(Input.GetAxis(settings.HorizontalInputName), 0, Input.GetAxis(settings.VerticalInputName));

#endif
		}

        public Vector3 Direction => _direction;

        public void Calib()
        {
            startXAcceleromètre = Input.acceleration.x;
            startZAcceleromètre = Input.acceleration.y;

            Debug.Log("ON EST OK ON CALIB !!!");
        }
	}
}