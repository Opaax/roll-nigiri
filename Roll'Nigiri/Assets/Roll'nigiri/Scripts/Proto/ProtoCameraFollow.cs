///-----------------------------------------------------------------
/// Author : Enguerran COBERT
/// Date : 01/02/2020 10:00
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri.Proto {
	public class ProtoCameraFollow : MonoBehaviour {

        [SerializeField] private Transform target;
		private void Start () {
			
		}
		
		private void Update () {

            transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
            transform.LookAt(target);
		}
	}
}