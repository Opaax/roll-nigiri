///-----------------------------------------------------------------
/// Author : Théophile BOHIN
/// Date : 02/02/2020 12:58
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri.Proto {

    [Serializable]
	public class Collectible : MonoBehaviour {

        public enum Type
        {
            Riz,
            Saumon,
            Thon,
            Onigiri,
            Nori
        }

        public Type collectibleType;
	}
}