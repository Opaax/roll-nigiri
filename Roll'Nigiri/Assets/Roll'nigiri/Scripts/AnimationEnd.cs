///-----------------------------------------------------------------
/// Author : Arthur RAFFAUD
/// Date : 01/02/2020 16:46
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri {
    public delegate void AnimIsFinish(Animator animator);
    public class AnimationEnd : MonoBehaviour {
        [SerializeField] private Animator animator;
        public static event AnimIsFinish animIsFinish;
        private void Start () {
			
		}
		
		private void Update () {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
            {
                animIsFinish(animator);
            }

        }
	}
}