///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 02/02/2020 05:39
///-----------------------------------------------------------------

using Cinemachine;
using Com.Onigiriz.RollNigiri.PlayerScripts;
using Com.Onigiriz.RollNigiri.Proto;
using UnityEngine;
using UnityEngine.Playables;

namespace Com.Onigiriz.RollNigiri.Managers
{
	public class GameManager : MonoBehaviour
	{
		[SerializeField] private UiManager uiManager = null;

		[SerializeField] private GameObject timeline = null;
		[SerializeField] private ParticleSystem particle = null;
		[SerializeField] private CinemachineVirtualCamera vCamTitle = null;
		[SerializeField] private CameraZoomByRatio vCamInGame = null;

		[SerializeField] private Player player = null;
		[SerializeField] private GameObject playerPrefab = null;

		[SerializeField] private Animator onigiriAnimator = null;
		[SerializeField] private GameObject decors = null;
		[SerializeField] private SoundManager soundManager = null;
		[SerializeField] private LevelGenerator LevelGenerator = null;

        public static bool pause = false;

		private void Start()
		{
			uiManager.OnPlayButton += UiManager_OnPlayButton;
			uiManager.OnQuitButton += UiManager_OnQuitButton;
			uiManager.OnTimerEnd += UiManager_OnTimerEnd;
			uiManager.OnPause += UiManager_OnPause;

			OnigiriLolManager.OnAnimEnd += OnigiriLolManager_OnAnimEnd;

			soundManager.PlaySound(soundManager.TitleSound);
		}

		private void UiManager_OnPause(UiManager uiManager)
		{
			//player.SetModeVoidFromExtern();
		}

		private void UiManager_OnTimerEnd(UiManager uiManager)
		{
			uiManager.varTestRice = 0;
			uiManager.varTestSalmon = 0;
			uiManager.varTestTuna = 0;
			uiManager.varTestNoriLeaves = 0;
			uiManager.varTestGoldRice = 0;

			player.SetModeVoidFromExtern();

			Collectible collectible = null;
			for (int i = player.transform.childCount - 1; i >= 0; i--)
			{
				collectible = player.transform.GetChild(i).GetComponent<Collectible>();
				switch (collectible.collectibleType)
				{
					case Collectible.Type.Riz:
						uiManager.varTestRice++;
						break;
					case Collectible.Type.Saumon:
						uiManager.varTestSalmon++;
						break;
					case Collectible.Type.Thon:
						uiManager.varTestTuna++;
						break;
					case Collectible.Type.Onigiri:
						uiManager.varTestGoldRice++;
						break;
					case Collectible.Type.Nori:
						uiManager.varTestNoriLeaves++;
						break;
					default:
						Debug.Log("C'est quoi lokl ??,,");
						break;
				}
			}

			uiManager.SetGameOver();
		}

		public void Genereted()
		{
			LevelGenerator.Generate();
		}
		private void OnigiriLolManager_OnAnimEnd()
		{
			decors.SetActive(false);
		}

		private void UiManager_OnPlayButton(UiManager uiManager)
		{
			if (uiManager.isPause)
			{
				player.Init();
				uiManager.SetHud();
				soundManager.PlaySound(soundManager.LevelSound);
			}

			timeline.GetComponent<PlayableDirector>().Play();
			vCamTitle.m_Priority = 14;
			onigiriAnimator.SetTrigger("FallTrigger");
		}

		private void UiManager_OnQuitButton(UiManager uiManager)
		{
			player.GetComponent<Collider>().enabled = false;

			vCamTitle.m_Priority = 16;
			uiManager.SetTitleCard();
			particle.Play();

			DestroyImmediate(player.transform.parent.gameObject);
			player = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity).GetComponentInChildren<Player>();

			vCamInGame.Init(player);

			decors.SetActive(true);
			onigiriAnimator.SetTrigger("SetUp");

			LevelGenerator.UnGenerate();
		}

		public void OnTimelineEnded()
		{
			soundManager.PlaySound(soundManager.LevelSound);
			soundManager.StopSound(soundManager.TitleSound);
			uiManager.SetHud();
			player.Init();
		}
	}
}