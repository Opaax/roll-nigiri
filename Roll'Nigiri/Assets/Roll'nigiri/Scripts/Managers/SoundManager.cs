///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 14/01/2020 11:45
///-----------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Com.Onigiriz.RollNigiri.Managers
{
	[Serializable]
	public class Sound
	{
		public string name = "Default";
		public AudioClip clip = null;
		public AudioMixer mixer = null;

        [HideInInspector] public AudioSource source = null;

        public bool loop;

		[Range(0f, 1f)] public float volume = 1f;
		[Range(-3f, 3f)] public float pitch = 1f;
	}

	public class SoundManager : MonoBehaviour
	{
		public static SoundManager instance;
		[SerializeField] private Sound[] sounds = null;

        public string MUSIQUE = "musique_resto";
        public string AMBIENT = "ambience_resto";
        public string LEVEL = "level_song";
        public string CHIME = "chimes_resto";
        public string CHOPSTICK = "chopstick";
        public string GONG = "gong";
        public string GOLDEN_RICE = "score_goldenricegrain";
        public string NORI = "score_nori";
        public string SALMON = "score_salmon";
        public string TUNA = "score_tuna";
        public string ONI = "oni_touch";
        public string RICE = "riceball";
        public string RICEGRAIN = "ricegrain_";

        [SerializeField] private AudioSource _titleSound = null;
        [SerializeField] private AudioSource _levelSound = null;
        [SerializeField] private AudioSource _pauseSound = null;
        [SerializeField] private AudioSource _gondSound = null;

        public AudioSource TitleSound => _titleSound;
        public AudioSource LevelSound => _levelSound;
        public AudioSource PauseSound => _pauseSound;
        public AudioSource GongSound => _gondSound;

        //public string RICE_2 = "riceball_2";
        //public string RICEGRAIN_2 = "riceball_2";
        //public string RICE_3 = "riceball_3";
        //public string RICEGRAIN_3 = "riceball_3";
        //public string RICE_4 = "riceball_4";
        //public string RICEGRAIN_4 = "riceball_4";
        //public string RICE_5 = "riceball_5";
        //public string RICEGRAIN_5 = "riceball_5";

         public void PlaySound (AudioSource audio, bool isLoop = false)
        {
            audio.Play();
            audio.loop = isLoop;
        }

        public void StopSound(AudioSource audio)
        {
            audio.Stop();
        }

        public void PauseAudio(AudioSource audio)
        {
            audio.Pause();
        }

        private void Awake()
		{
			if (instance == null)
				instance = this;
			else
			{
				Destroy(gameObject);
				return;
			}

			//Sound sound;
			//for (int i = sounds.Length - 1; i >= 0; i--)
			//{
			//	sound = sounds[i];
			//	sound.source = gameObject.AddComponent<AudioSource>();
			//	sound.source.clip = sound.clip;
			//	sound.source.pitch = sound.pitch;
			//	sound.source.volume = sound.volume;
			//	sound.source.loop = sound.loop;
			//}

			// Allows AudioManager to not destroy from the loading of a NewScene
			//DontDestroyOnLoad(gameObject);
		}

        private void Start()
        {
            //Play(AMBIENT);
            //Play(MUSIQUE);
        }

        //public void Play(string clipName)
		//{
        //    //allAudio.Add(lNewAudio);
        //    //// finds the matching sound into the sounds array
		//	//Sound soundObject = Array.Find(sounds, sound => sound.name == clipName);
        //    //
		//	//// check if misspelled
		//	//if (soundObject == null)
		//	//{
		//	//	Debug.Log("<color=green><size=21>Sound in sounds array " + clipName + " Not found!!</size></color>");
		//	//	return;
		//	//}
        //    //
		//	////soundObject.source.Play();
		//	//soundObject.source.PlayOneShot(soundObject.clip);
		//}
	}
}