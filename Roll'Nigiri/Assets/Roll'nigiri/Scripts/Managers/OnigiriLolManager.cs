///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 02/02/2020 11:31
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri.Managers
{
	public class OnigiriLolManager : MonoBehaviour
	{
		public static event Action OnAnimEnd;

		public void AnimEnd()
		{
			OnAnimEnd?.Invoke();
		}
	}
}