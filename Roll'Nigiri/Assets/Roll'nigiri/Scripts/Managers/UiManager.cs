///-----------------------------------------------------------------
/// Author : Arthur RAFFAUD
/// Date : 01/02/2020 02:49
///-----------------------------------------------------------------

using Com.Onigiriz.RollNigiri.Ui;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.Onigiriz.RollNigiri.Managers {
	public delegate void UIManagerEvent(UiManager uiManager);

	public class UiManager : MonoBehaviour {
        [SerializeField] private Canvas titleCard;
        [SerializeField] private Canvas hud;
        [SerializeField] private Canvas pause;
        [SerializeField] private Canvas gameOver;

        private List<Canvas> canvasList = new List<Canvas>();

        private Action DoAction;
        private Action DoActionCanvas;

        private int highScore = 0;
        private float gameOverScoreCounter = 0;
        private float elapsedTime = 60;

        private string timerText;

        private string nameButton;

        private Canvas actualCanvas;

        private int numberRice = 0;
        private int numberSalmon = 0;
        private int numberTuna = 0;
        private int numberGoldRice = 0;
        private int numberNoriLeaves = 0;

        [SerializeField] private Text numberRiceText;
        [SerializeField] private Text numberSalmonText;
        [SerializeField] private Text numberTunaText;
        [SerializeField] private Text numberGoldRiceText;
        [SerializeField] private Text numberNoriLeavesText;

        [SerializeField] private Text scoreRiceText;
        [SerializeField] private Text scoreSalmonText;
        [SerializeField] private Text scoreTunaText;
        [SerializeField] private Text scoreGoldRiceText;
        [SerializeField] private Text scoreNoriLeavesText;

        [SerializeField] private int scoreRice = 10;
        [SerializeField] private int scoreSalmont = 100;
        [SerializeField] private int scoreTuna = 100;
        [SerializeField] private int scoreGoldRice = 150;
        [SerializeField] private int scoreNoriLeaves = 200;

        public int varTestRice = 50;
		public int varTestSalmon = 40;
		public int varTestTuna = 30;
		public int varTestGoldRice = 20;
		public int varTestNoriLeaves = 10;

		public bool isPause = false;

        private float total = 0;

        [SerializeField] private Text totalScore;

		public event UIManagerEvent OnPlayButton;
		public event UIManagerEvent OnQuitButton;
		public event UIManagerEvent OnTimerEnd;
		public event UIManagerEvent OnPause;

        private void Start () {
            ButtonAction.buttonIsOn += ButtonIsOn;

            canvasList.Add(titleCard);
            canvasList.Add(hud);
            canvasList.Add(pause);
            canvasList.Add(gameOver);

            SetTitleCard();
            SetModeVoidCanvas();
        }
        private void SetModeVoid()
        {

            DoAction = DoActionVoid;
        }

        private void DoActionVoid()
        {

        }

        private void SetModeVoidCanvas()
        {

            DoActionCanvas = DoActionVoidCanvas;
        }

        private void DoActionVoidCanvas()
        {

        }

        private void Update () {
            DoAction();
            DoActionCanvas();
        }

        public void SetTitleCard()
        {
            //elapsedTime = 0;
            gameOverScoreCounter = 0;

            CanvasActif(titleCard);
            //titleCard.gameObject.GetComponentInChildren<Text>().text = "High Score : " + highScore;
            DoAction = DoActionTitleCard;
        }

        private void DoActionTitleCard()
        {

        }

        public void SetPause()
        {
            GameManager.pause = isPause = true;
			OnPause?.Invoke(this);

			SoundManager.instance.PauseAudio(SoundManager.instance.LevelSound);
			SoundManager.instance.PlaySound(SoundManager.instance.PauseSound);
            CanvasActif(pause);
            DoAction = DoActionPause;
        }

        private void DoActionPause()
        {

        }

        public void SetHud()
        {
            CanvasActif(hud);
            DoAction = DoActionHud;
        }

        private void DoActionHud()
        {
            elapsedTime -= Time.deltaTime;
            HudTimer(elapsedTime);
        }

        public void SetGameOver()
        {
            CanvasActif(gameOver);
            DoAction = DoActionGameOver;

            score = varTestRice;
            multiplicator = scoreRice;

            displayScore = 0;
            displayNumber = 0;
            
            numberUI = numberRiceText;
            scoreUI = scoreRiceText;

            StartCoroutine(NumberUpdater());
            StartCoroutine(ScoreUpdater());
        }

        private void DoActionGameOver()
        {
            //elapsedTime += Time.deltaTime;
            if (displayScore + total == varTestRice * scoreRice)
            {
                total += displayScore;
                StartCoroutine(TotalUpdater());

                score = varTestSalmon;
                multiplicator = scoreSalmont;

                displayScore = 0;
                displayNumber = 0;

                numberUI = numberSalmonText;
                scoreUI = scoreSalmonText;

                StartCoroutine(NumberUpdater());
                StartCoroutine(ScoreUpdater());
            }
            else if (displayScore + total == varTestRice * scoreRice + varTestSalmon * scoreSalmont)
            {
                total += displayScore;
                StartCoroutine(TotalUpdater());

                score = varTestTuna;
                multiplicator = scoreTuna;

                displayScore = 0;
                displayNumber = 0;

                numberUI = numberTunaText;
                scoreUI = scoreTunaText;

                StartCoroutine(NumberUpdater());
                StartCoroutine(ScoreUpdater());
            }
            else if (displayScore + total == varTestRice * scoreRice + varTestSalmon * scoreSalmont + varTestTuna * scoreTuna)
            {
                total += displayScore;
                StartCoroutine(TotalUpdater());

                score = varTestGoldRice;
                multiplicator = scoreGoldRice;

                displayScore = 0;
                displayNumber = 0;

                numberUI = numberGoldRiceText;
                scoreUI = scoreGoldRiceText;

                StartCoroutine(NumberUpdater());
                StartCoroutine(ScoreUpdater());
            }
            else if (displayScore + total == varTestRice * scoreRice + varTestSalmon * scoreSalmont + varTestTuna * scoreTuna + varTestGoldRice * scoreGoldRice)
            {
                total += displayScore;
                StartCoroutine(TotalUpdater());

                score = varTestNoriLeaves;
                multiplicator = scoreNoriLeaves;

                displayScore = 0;
                displayNumber = 0;

                numberUI = numberNoriLeavesText;
                scoreUI = scoreNoriLeavesText;

                StartCoroutine(NumberUpdater());
                StartCoroutine(ScoreUpdater());
            }
            else if (displayScore + total == varTestRice * scoreRice + varTestSalmon * scoreSalmont + varTestTuna * scoreTuna + varTestGoldRice * scoreGoldRice + varTestNoriLeaves * scoreNoriLeaves)
            {
                total += displayScore;
                StartCoroutine(TotalUpdater());
            }

			if (displayTotal == total && total != 0)
			{
				
				gameOver.gameObject.GetComponent<Animator>().SetBool("ScoreIsDone", true);
			}
		}

        //===========================================================

        //You need a variable for the actual score
        public int score;
        public int multiplicator;
        public int displayNumber;
        public int displayScore;
        public int displayTotal;
        private Text numberUI;
        private Text scoreUI;
        private IEnumerator NumberUpdater()
        {
            while (true)
            {
                if (displayNumber < score)
                {
                    displayNumber++; //Increment the display score by 1
                    numberUI.text = "x " + displayNumber.ToString(); //Write it to the UI
                }
                yield return new WaitForSeconds(0.0025f); // I used .2 secs but you can update it as fast as you want
            }
        }

        private IEnumerator ScoreUpdater()
        {
            while (true)
            {
                if (displayScore < score * multiplicator)
                {
                    displayScore = displayNumber * multiplicator; //Increment the display score by 1
                    scoreUI.text = "= " + displayScore.ToString(); //Write it to the UI
                }
                yield return new WaitForSeconds(0.0025f); // I used .2 secs but you can update it as fast as you want
            }
        }

        private IEnumerator TotalUpdater()
        {
            while (true)
            {
                if (displayTotal < total)
                {
                    displayTotal += 1; //Increment the display score by 1
                    totalScore.text = "TOTAL : " + displayTotal.ToString(); //Write it to the UI
                }
                yield return new WaitForSeconds(0.0005f); // I used .2 secs but you can update it as fast as you want
            }
        }
        //=========================================================

        private void ButtonIsOn(string btnName)
        {
            nameButton = btnName;
            SetCanvasWhenPreviousEnd();


            /*Animator animator = actualCanvas.GetComponent<Animator>();

            animator.SetBool("isEnd", true);

            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
            {
                

                if (btnName == "PlayButton")
                {
                    Debug.Log("hud");
                    SetHud();
                }
                else if (btnName == "PauseButton")
                {
                    Debug.Log("pause");
                    SetPause();
                }
                else if (btnName == "QuitButton")
                {
                    Debug.Log("title");
                    SetTitleCard();
                }
            }*/
        }
        public void SetCanvasWhenPreviousEnd()
        {
            DoActionCanvas = DoActionCanvasWhenPreviousEnd;
        }
        private void DoActionCanvasWhenPreviousEnd()
        {
            Animator animator = actualCanvas.GetComponent<Animator>();
            animator.SetBool("isEnd", true);

            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
            {
                if (nameButton == "PlayButton")
                {
					OnPlayButton?.Invoke(this);
                    //SetHud();
                }
                else if (nameButton == "resume")
                {
                    GameManager.pause = isPause = false;
                    Debug.Log("resume");
                    SetHud();
                }
                else if (nameButton == "PauseButton")
                {
                    Debug.Log("pause");
                    SetPause();
                }
                else if (nameButton == "QuitButton")
                {
                    elapsedTime = 10;
                    GameManager.pause = isPause = false;
                    OnQuitButton?.Invoke(this);
                    //SetTitleCard();
                }

                SetModeVoidCanvas();
            }
        }

        private void CanvasActif(Canvas canvasActif)
        {
            if (actualCanvas != null && actualCanvas.GetComponent<Animator>().GetBool("isEnd")) actualCanvas.GetComponent<Animator>().SetBool("isEnd", false);
            for (int i = canvasList.Count - 1; i >= 0; i--)
            {
                //if (canvasList[i].gameObject.GetComponent<Animator>().GetBool("isEnd")) canvasList[i].gameObject.GetComponent<Animator>().SetBool("isEnd", false);
                canvasList[i].gameObject.SetActive(false);
            }
            canvasActif.gameObject.SetActive(true);
            actualCanvas = canvasActif;
        }

        private void HudTimer(float timer)
        {

            if (Mathf.Round(timer % 60) < 10) timerText = Mathf.Ceil(timer / 60) - 1 + ":0" + Mathf.Round(timer % 60);
            else timerText = Mathf.Ceil(timer / 60) - 1 + ":" + Mathf.Round(timer % 60);

            hud.gameObject.GetComponentInChildren<Text>().text = timerText;

			if(timerText == "0:00")
			{
				SoundManager.instance.PlaySound(SoundManager.instance.GongSound);
				SoundManager.instance.StopSound(SoundManager.instance.LevelSound);
				SoundManager.instance.PlaySound(SoundManager.instance.TitleSound);
                elapsedTime = 10;
				OnTimerEnd?.Invoke(this);
			}
        }
	}
}