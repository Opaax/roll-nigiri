///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 01/02/2020 15:16
///-----------------------------------------------------------------

using Cinemachine;
using Com.Onigiriz.RollNigiri.PlayerScripts;
using System.Collections;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri
{
	[RequireComponent(typeof(CinemachineVirtualCamera))]
	public class CameraZoomByRatio : MonoBehaviour
	{
		[SerializeField] private CameraZoomByRatioSettings settings = null;

		private CinemachineVirtualCamera vCam = null;
		private CinemachineTransposer vCamBody = null;

		private SphereCollider playerCollider = null;

		private float startRadius = 1f;
		private float radius = 1f;

        private int currentStepToUp = 1;

		private void Start()
		{
			vCam = GetComponent<CinemachineVirtualCamera>();
			vCamBody = vCam.GetCinemachineComponent<CinemachineTransposer>();
			playerCollider = vCam.Follow.GetComponentInChildren<Player>().GetComponent<SphereCollider>();

			vCamBody.m_FollowOffset.y = settings.MinYFollowOffset;
			startRadius = radius = playerCollider.radius;

			vCam.Follow.GetComponentInChildren<Player>().OnGrow += Player_OnGrow;
		}

		public void Init(Player player)
		{
			vCam.m_Follow = player.transform;
			playerCollider = player.GetComponent<SphereCollider>();

			vCamBody.m_FollowOffset.y = settings.MinYFollowOffset;
			startRadius = radius = playerCollider.radius;

			vCam.Follow.GetComponentInChildren<Player>().OnGrow += Player_OnGrow;
		}

		private void Player_OnGrow(Player player)
		{
            if (currentStepToUp > settings.MaxNBStep) return;

			radius = playerCollider.radius;

            float lRatio = (float)currentStepToUp / (float)settings.MaxNBStep;

            currentStepToUp++;

            if(vCamBody.m_FollowOffset.y < settings.MaxYFollowOffset)
            {
                if(currentStepToUp > settings.MaxNBStep)
                {
                    vCamBody.m_FollowOffset.y = settings.MaxYFollowOffset;
                    return;
                }

                float lNextStep = vCamBody.m_FollowOffset.y + ((settings.MaxYFollowOffset - settings.MinYFollowOffset) / settings.MaxNBStep);
                StartCoroutine(SmoothCameraFollowOffset(vCamBody.m_FollowOffset.y, lNextStep));
            }
            else
            {
                vCamBody.m_FollowOffset.y = settings.MaxYFollowOffset;
            }
		}

        private IEnumerator SmoothCameraFollowOffset (float prevPosY, float nextPosY)
        {
            float lPrevPos = prevPosY;
            float lNextPos = nextPosY;
            float lRatio = 0;
            float lElapsedTime = 0;
            
            while(lRatio <= 1)
            {
                lElapsedTime += Time.deltaTime;
                lRatio = lElapsedTime / 1.5f;
                vCamBody.m_FollowOffset.y = Mathf.Lerp(lPrevPos, lNextPos,lRatio);

                yield return null;
            }
            
        }
	}
}