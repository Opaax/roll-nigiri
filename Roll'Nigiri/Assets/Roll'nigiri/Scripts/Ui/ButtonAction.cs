///-----------------------------------------------------------------
/// Author : Arthur RAFFAUD
/// Date : 01/02/2020 02:27
///-----------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI;

namespace Com.Onigiriz.RollNigiri.Ui {
    public delegate void ButtonIsOn(string name);
    public class ButtonAction : MonoBehaviour {
        [SerializeField] private Button button;
        [SerializeField] private string buttonName = "Button";

        public static event ButtonIsOn buttonIsOn;
        private void Start()
        {
            Button btn = button.GetComponent<Button>();
            button.onClick.AddListener(ButtonOn);
        }

        private void ButtonOn()
        {
            //Debug.Log(button.name + " is On");
            buttonIsOn(buttonName);
        }

        private void Update()
        {

        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(ButtonOn);
        }
    }
}