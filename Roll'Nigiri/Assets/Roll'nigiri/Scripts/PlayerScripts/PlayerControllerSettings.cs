///-----------------------------------------------------------------
/// Author : Enguerran COBERT
/// Date : 01/02/2020 12:36
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri.PlayerScripts {
    [CreateAssetMenu(
        fileName = "PlayerController",
        menuName = "Roll'Nigiri/PlayerController")]
	public class PlayerControllerSettings : ScriptableObject {
        [Header("InputsName")]
        [SerializeField] private string _horizontalInputName = "";
        [SerializeField] private string _verticalInputName = "";
        [Space]
        [Header("Settings")]
        [SerializeField, Range(0.2f, 0.6f)] private float _overOffSetRotation = 0f; 
        [SerializeField, Range(-1f, -0.8f)] private float _overRotationDelta = 0f;

        public float OverOffSetRotation => _overOffSetRotation;
        public float OverRotationDelta => _overRotationDelta;
        public string HorizontalInputName => _horizontalInputName;
        public string VerticalInputName => _verticalInputName;
	}
}