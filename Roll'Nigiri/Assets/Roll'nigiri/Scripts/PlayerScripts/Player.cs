///-----------------------------------------------------------------
/// Author : Théophile BOHIN
/// Date : 01/02/2020 00:02
///-----------------------------------------------------------------

using Com.Onigiriz.RollNigiri.Managers;
using Com.Onigiriz.RollNigiri.Proto;
using Com.Onigiriz.RollNigiri.RollNigiri.Proto;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri.PlayerScripts {
	public delegate void PlayerEvent(Player player);

	public class Player : StateMachine {

        [Header("Physics")]

        [SerializeField] private float gravity = 9.81f;
        [SerializeField] private Transform groundRaycastStart;
        [SerializeField] private float raycastDistance = 1.1f;
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private SphereCollider parentToGrowCollider = null;


        [Header("Movement")]
        [SerializeField] private float speed;
        [SerializeField] private float airborneSpeed;
        [SerializeField] private AnimationCurve accelerationCurve;
        [SerializeField] private AnimationCurve deccelerationCurve;
        [SerializeField] private float timeToAccelerate;
        [SerializeField] private float timeToDeccelerate;
        [SerializeField] private Quaternion firstRotation;
        [SerializeField] private Quaternion secoundRotation;
        [SerializeField] private float rotationSpeed = 6f;

        [SerializeField] private float raycastOffsetFactor = 0.9f;

        [SerializeField] private PlayerController controller;
        
        private Vector3 origin = Vector3.zero;
        private Vector3 velocity = Vector3.zero;
        private Vector3 acceleration = Vector3.zero;
        private Vector3 movementDirection = Vector3.zero;

        private Camera mainCam = null;
        private SphereCollider toGrowCollider = null;

        private Accelerometer accelerometor;

        private Rigidbody rb = null;
        private Rigidbody2D rB2D = null;

		public void Init()
		{
			SetModeNormal();
		}

		private bool _isOnWall = false;
        private bool _isGrounded = false;
        private float layerMask = 0;
        private float force = 9;
        private Vector4 angularVelocity = Vector4.zero;
        private string collectableTag = "Collectable";
        private string collectedLayerName = "Collected";

        public bool IsGrounded { get => _isGrounded; private set => _isGrounded = value; }
        public bool IsOnWall { get => _isOnWall; private set => _isOnWall = value; }

        [SerializeField] LayerMask collectedLayer;
        [SerializeField, Range(0.01f, 0.2f)] float offSetRiz;
        [SerializeField] private float centerSphereRadius;
        [SerializeField] private float offsetToRemoveToParentCollider;
        [SerializeField] private List<AudioSource> soundPlayer = new List<AudioSource>();

        private SoundManager soundManager = null;
        private string soundCollectName;

        public event PlayerEvent OnGrow;

        private int counterCollider = 0;
        private int countedCollider = 50;

		private List<GameObject> collectedObjects = new List<GameObject>();
		private List<GameObject> activeCollidersObjects = new List<GameObject>();

        private AudioSource currentAudio = null;

        private void Start()
        {
            mainCam = Camera.main;
            rb = GetComponentInParent<Rigidbody>();
            toGrowCollider = GetComponent<SphereCollider>();

            accelerometor = GetComponent<Accelerometer>();
            soundManager = SoundManager.instance;

			DoAction = DoActionVoid;
        }

        private void FixedUpdate()
        {
            DoAction();
        }

        private void LateUpdate()
        {
            if (++counterCollider >= countedCollider)
            {
                counterCollider = 0;
                Optimize();
            }
        }

		public void SetModeVoidFromExtern()
		{
			DoAction = DoActionVoid;
		}

        protected override void SetModeNormal()
        {
            base.SetModeNormal();
        }

        private void DetectWall()
        {
            IsOnWall = false;
            bool detectionRight = Physics.Raycast(origin, Vector3.right * 1 / 2, raycastDistance);
            bool detectionLeft = Physics.Raycast(origin, Vector3.left * 1 / 2, raycastDistance);
        }

        private void Move()
        {

#if UNITY_ANDROID && !UNITY_EDITOR
            float xVelocity = Accelerometer.staticTilt.x;
            float zVelocity = Accelerometer.staticTilt.z;
#else

            float xVelocity = GetBothAxis().x;
            float zVelocity = GetBothAxis().z;
#endif
            if (!GameManager.pause)
            {
                rb.velocity += new Vector3(xVelocity, 0, zVelocity).normalized;
                rb.velocity *= speed * Time.deltaTime;
            }
            else
            {
                rb.velocity = Vector3.zero;
            }

            float factor = Mathf.PI * 2;

            firstRotation = Quaternion.AngleAxis(factor * zVelocity, Vector3.right);
            secoundRotation = Quaternion.AngleAxis(- factor * xVelocity, Vector3.forward);

            transform.rotation = firstRotation * secoundRotation * transform.rotation;

            if(Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                    accelerometor.Calibrate();
            }
        }

        protected void Optimize()
        {
			for (int i = activeCollidersObjects.Count - 1; i >= 0; i--)
			{
				Transform activeCollTransform = activeCollidersObjects[i].transform;
                
                Vector3 lDistance = activeCollTransform.transform.position - transform.position;

                if (lDistance.magnitude < toGrowCollider.radius - 0.1f)
                {
                    Destroy(activeCollTransform.GetComponent<BoxCollider>());
					activeCollidersObjects.RemoveAt(i);
                }
            }
        }

        protected override void DoActionNormal()
        {
            Move();
        }

        private Vector3 GetBothAxis()
        {
            Vector3 axis = Vector3.zero;

            axis.x = controller.Direction.x;
            axis.z = controller.Direction.z;

            return axis;
        }

        // method called when we hit a rice grain
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(collectableTag))
            {
                GameObject lContactObject = other.gameObject;
				bool isChain = lContactObject.GetComponent<ChuckNori>() != null;

                Vector3 lDirectionraycast = lContactObject.transform.position - transform.position;
                int layerToIgnor = LayerMask.GetMask(collectedLayerName);
                if (Physics.Raycast(transform.position, lDirectionraycast, out RaycastHit hitInfo, raycastDistance * 2, layerToIgnor))
                {
                    GameObject moi = hitInfo.collider.gameObject;
                    lContactObject.layer = LayerMask.NameToLayer(collectedLayerName);
                    lContactObject.GetComponent<BoxCollider>().isTrigger = true;
                    lContactObject.transform.position = moi.transform.position + Vector3.Normalize(lDirectionraycast) * offSetRiz;
                    lContactObject.transform.SetParent(transform);
                    Collider lColliderOfContactObject = lContactObject.GetComponent<BoxCollider>();

                    Destroy(hitInfo.collider);
	 
					if (isChain)
						lContactObject.transform.rotation = Quaternion.LookRotation(lContactObject.transform.up, -lDirectionraycast);
					else
						lContactObject.transform.LookAt(transform.parent.position);

                    Destroy(lContactObject.GetComponent<Rigidbody>());
                    RecomputeColiderSize(lContactObject.transform);
                }
                else
                {
                    Destroy(lContactObject.GetComponent<Rigidbody>());

                    lContactObject.layer = LayerMask.NameToLayer(collectedLayerName);
                    Collider lColliderOfContactObject = lContactObject.GetComponent<BoxCollider>();
                    lColliderOfContactObject.isTrigger = true;
                    lContactObject.transform.position = transform.position /* transform.position*/ + Vector3.Normalize(lDirectionraycast) * centerSphereRadius;
                    lContactObject.transform.SetParent(transform);

					if (isChain)
						lContactObject.transform.rotation = Quaternion.LookRotation(lContactObject.transform.up, -lDirectionraycast);
					else
						lContactObject.transform.LookAt(transform.parent.position);
                }
                counterCollider++;

                PlaySound();

				collectedObjects.Add(lContactObject);
				activeCollidersObjects.Add(lContactObject);
            }
        }

        private void PlaySound()
        {
            if (currentAudio != null)
            {
                if (currentAudio.isPlaying) return;

                int lRandom = UnityEngine.Random.Range(0, soundPlayer.Count);
                currentAudio = soundPlayer[lRandom];
                currentAudio.Play();

			}
			else
            {
                int lRandom = UnityEngine.Random.Range(0, soundPlayer.Count);
                currentAudio = soundPlayer[lRandom];
                currentAudio.Play();
            }
        }

        private void RecomputeColiderSize(Transform newChild)
        {
            Vector3 lDistance = newChild.position - transform.position;

            if(lDistance.magnitude > toGrowCollider.radius)
            {
                toGrowCollider.radius = lDistance.magnitude;
                parentToGrowCollider.radius = lDistance.magnitude - offsetToRemoveToParentCollider;

				OnGrow?.Invoke(this);
            }
        }
    }
}