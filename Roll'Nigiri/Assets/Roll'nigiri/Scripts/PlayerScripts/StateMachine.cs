///-----------------------------------------------------------------
/// Author : Théophile BOHIN
/// Date : 01/02/2020 00:57
///-----------------------------------------------------------------

using System;
using UnityEngine;

namespace Com.Onigiriz.RollNigiri.PlayerScripts {
	public class StateMachine : MonoBehaviour {

        protected Action DoAction;

        protected virtual void DoActionNormal() { }
        virtual protected void SetModeNormal() {

            DoAction = DoActionNormal;
        }

        protected virtual void DoActionVoid() { }
        protected void SetModeVoid() {

            DoAction = DoActionVoid;
        }
        protected virtual void DoActionFall() { }
        protected void SetModeFall() {
            DoAction = DoActionFall;
        }
    }
}