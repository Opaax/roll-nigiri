///-----------------------------------------------------------------
/// Author : Théophile BOHIN
/// Date : 01/02/2020 19:34
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri.PlayerScripts {
	public class Accelerometer : MonoBehaviour {

        [SerializeField, Range(3f, 300f)] private float speed = 10f;

        public bool isFlat = false;
        private Rigidbody rb = null;
        private Vector3 fixedTilt;
        private Vector3 tilt;
        public static Vector3 staticTilt;

        private float startZAcceleromètre = 0;
        private float startXAcceleromètre = 0;
        private float gravity = 9.81f;
        private float offset = 0.1f;

        private Quaternion calibrationQuat;

        private void Start()
        {
            rb = GetComponentInParent<Rigidbody>();
            Calibrate();
        }

        public void Calibrate()
        {
            Vector3 accel = Input.acceleration;

            Quaternion rotateTo = Quaternion.FromToRotation(new Vector3(0.0f, -1.0f, 0.0f), accel);

            calibrationQuat = Quaternion.Inverse(rotateTo);

            startXAcceleromètre = 0;
            startZAcceleromètre = -0.5f;
        }

        private void Update()
        {
            staticTilt = GetAccelero();
        }

        public Vector3 GetAccelero()
        {
            tilt = Input.acceleration;
            fixedTilt = calibrationQuat * tilt;

            if (tilt.y > startZAcceleromètre + offset)
            {
                tilt.z = 1;
            }
            else if (tilt.y < startZAcceleromètre - offset)
            {
                tilt.z = -1;
            }
            else tilt.z = 0;

            if (tilt.x > startXAcceleromètre + offset)
            {
                tilt.x = 1;
            }
            else if (tilt.x < startXAcceleromètre - offset)
            {
                tilt.x = -1;
            }
            else tilt.x = 0;

            tilt.y = -gravity;
            //Debug.Log(tilt);

            return tilt;
        }

    }
}