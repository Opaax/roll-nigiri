///-----------------------------------------------------------------
/// Author : Enguerran COBERT
/// Date : 02/02/2020 05:18
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri {
    [CreateAssetMenu(
        menuName = "Roll'Nigiri/LevelGeneratorSettings",
        fileName = "LevelGeneratorSettings",
        order = 0
    )]
    public class LevelGeneratorSettings : ScriptableObject {
        [Header("Prefabs")]
        [SerializeField] private GameObject _ricePrefab = null;
        [SerializeField] private GameObject _sushiPrefab = null;
        [SerializeField] private GameObject _tunaPrefab = null;
        [SerializeField] private GameObject _noriPrefab = null;
        [Space]
        [Header("Specs")]
        [SerializeField] private int _nbRiceToSpawn;
        [SerializeField] private int _nbSushiToSpawn;
        [SerializeField] private int _nbTunaToSpawn;
        [SerializeField] private int _nbNoriToSpawn;

        public int NbRiceToSpawn => _nbRiceToSpawn;
        public int NbSushiToSpawn => _nbSushiToSpawn;
        public int NbTunaToSpawn => _nbTunaToSpawn;
        public int NbNoriToSpawn => _nbNoriToSpawn;

        public GameObject RicePrefab => _ricePrefab;
        public GameObject SushiPrefab => _sushiPrefab;
        public GameObject TunaPrefab => _tunaPrefab;
        public GameObject NoriPrefab => _noriPrefab;
	}
}