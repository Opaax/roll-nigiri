///-----------------------------------------------------------------
/// Author : Joël VOIGNIER
/// Date : 01/02/2020 16:15
///-----------------------------------------------------------------

using UnityEngine;

namespace Com.Onigiriz.RollNigiri
{
	[CreateAssetMenu(
		menuName = "Roll'Nigiri/CameraZoomByRatioSettings",
		fileName = "CameraZoomByRatioSettings",
		order = 0
	)]

	public class CameraZoomByRatioSettings : ScriptableObject
	{
		[SerializeField] private float _minYFollowOffset = 10f;
		[SerializeField] private float _maxYFollowOffset = 20f;
		[SerializeField] private int _NStepToUp = 6;
		[SerializeField] private AnimationCurve _animationOffSet = null;

		public float MinYFollowOffset { get => _minYFollowOffset; }
		public float MaxYFollowOffset { get => _maxYFollowOffset; }
		public AnimationCurve AnimationOffSet { get => _animationOffSet; }
		public int MaxNBStep { get => _NStepToUp; }
	}
}